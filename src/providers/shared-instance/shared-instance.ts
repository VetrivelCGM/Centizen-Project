import { Injectable } from '@angular/core';

/*
  Generated class for the SharedInstanceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SharedInstanceProvider {

  data: any

  constructor() {
    console.log('Hello SharedInstanceProvider Provider');
  }

  set(data){
    this.data = data;
  }

}
