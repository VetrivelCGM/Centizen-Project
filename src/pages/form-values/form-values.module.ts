import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormValuesPage } from './form-values';

@NgModule({
  declarations: [
    FormValuesPage,
  ],
  imports: [
    IonicPageModule.forChild(FormValuesPage),
  ],
})
export class FormValuesPageModule {}
