import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedInstanceProvider } from '../../providers/shared-instance/shared-instance';

/**
 * Generated class for the FormValuesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-values',
  templateUrl: 'form-values.html',
})
export class FormValuesPage {

  valuesFromParantClass: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public shared: SharedInstanceProvider) {

    this.valuesFromParantClass = this.shared.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormValuesPage');
  }
  

}
