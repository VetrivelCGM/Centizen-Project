import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailValidator } from '../../Validators/EmailValidator';

import { SharedInstanceProvider } from '../../providers/shared-instance/shared-instance';
import { FormValuesPage } from '../form-values/form-values';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  demoForm: any;

  touchedModel: boolean;

  constructor(public navCtrl: NavController,public sharedIns: SharedInstanceProvider,public formBuilder: FormBuilder  ) {

    this.demoForm = formBuilder.group({
      "options": ['A',Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required]), EmailValidator.checkEmail],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12), Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,12}$')])],
    });

  }
  gotoNextPage()
  {

    var obj = {
      email: this.demoForm.controls['email'].value,
      password: this.demoForm.controls['password'].value,
      perimum: this.demoForm.controls['options'].value
    }

    this.sharedIns.set(obj);

    this.navCtrl.push(FormValuesPage);
  }
  ionViewWillAppear()
  {
    if(this.sharedIns.data != undefined)
    {
      this.demoForm.controls['email'].setValue('');
      this.demoForm.controls['options'].setValue('');
      this.demoForm.controls['password'].setValue('');
  
      this.demoForm.controls['email'].touched = false;
      this.demoForm.controls['options'].touched = false;
      this.demoForm.controls['password'].touched = false;
    }
  }
  clearAllFields()
  {
    this.demoForm.controls['email'].setValue('');
    this.demoForm.controls['options'].setValue('A');
    this.demoForm.controls['password'].setValue('');

    this.demoForm.controls['email'].touched = false;
    this.demoForm.controls['options'].touched = false;
    this.demoForm.controls['password'].touched = false;
    this.demoForm.valid = true;
  }
}
