import { FormControl } from '@angular/forms';

export class EmailValidator {

  static checkEmail(control: FormControl): any {

    return new Promise(resolve => {

      if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
        console.log("valid email");
        resolve(null);
      } else if (control.value == "" || control.value == null) {
        console.log("email empty");

        resolve(null);
      } else
        console.log("email not valid");

      resolve({

        'invalidEmailAddress': true
      });
    });
  }

}
